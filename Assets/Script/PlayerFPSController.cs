using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSController : MonoBehaviour
{
    public GameObject camerasParent;
    public float walkSpeed = 5f;                // Walk speed
    public float hRotationSpeed = 100f;      //Player rotates along y axis
    public float vRotationspeed = 80f;        // Cam rotates along x axis

    // Use this for initialization
    private void Start()
    {
        // Hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Minion").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        movement();

        // Rotation
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationspeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);
        camerasParent.transform.Rotate(-vCamRotation, 0f, 0f);
    }

    private void movement()
    {
        // C�digo de movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));
    }

}
