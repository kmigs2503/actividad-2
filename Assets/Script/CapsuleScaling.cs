using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
     private Vector3 axes;    // posibles ejes de escalado
    public float scaleUnit;   // velocidad de escalado

    // Start is called before the first frame update
    void Start()
    {
        // Acotaci�n de los valores de escalado al valor unitario [-1, 1]
        axes = CapsuleMovement.ClampVector3(axes);

    }

    // Update is called once per frame
    void Update()
    {
        // la escala, al  contrario que la rotaci�n y el movimiento, es acumulativo
        // Lo que quiere decir decir que debbemos a�adir  el nuevo valor  de la escala, al valor anterior.
        transform.localScale += axes * (scaleUnit * Time.deltaTime);
    }
}


